import { Injectable } from '@angular/core';
import { AuthService, GoogleLoginProvider } from 'angularx-social-login';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Token } from '../models/token';
import { HttpService } from './http.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private onLoggedInChange$ = new Subject<boolean>();
  onLoggedInChange: Observable<boolean>;
  constructor(
    private httpService: HttpService,
    private tokenService: TokenService,
    private authService: AuthService
  ) {
    this.onLoggedInChange = this.onLoggedInChange$.asObservable();
  }

  signInWithGoogle(): Observable<any> {
    return new Observable((observer) => {
      let signIn$: Subscription;
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
        .then(userData => {
          signIn$ = this.signIn(userData.idToken).subscribe((token: Token) => {
            this.tokenService.setSessionToken(token.accessToken);
            this.tokenService.setRefreshToken(token.refreshToken);
            observer.next(token);
            observer.complete();
            this.onLoggedInChange$.next(true);
          }, err => {
            observer.error(err);
            observer.complete();
          });
        }).catch(err => {
          observer.error(err);
          observer.complete();
        });

      return {
        unsubscribe() {
          signIn$.unsubscribe();
        }
      };
    });
  }

  private signIn(idToken: string): Observable<Token> {
    return this.httpService.post('authentication/login/google', { idToken });
  }

  signOut(): Observable<any> {
    const sessionToken = this.tokenService.getSessionToken();

    if (sessionToken) {
      return this.httpService.post('authentication/logout', {}).pipe(
        tap(() => {
          this.setToSignedOutState();
        })
      );
    }

    this.setToSignedOutState();
    return of();
  }

  isLoggedIn(): Observable<boolean> {
    return of(!!this.tokenService.getSessionToken());
  }

  setLoggedInStatus(isLoggedIn: boolean): void {
    this.onLoggedInChange$.next(isLoggedIn);
  }

  private setToSignedOutState(): void {
    this.tokenService.removeSessionToken();
    this.tokenService.removeRefreshToken();
    this.onLoggedInChange$.next(false);
  }
}
