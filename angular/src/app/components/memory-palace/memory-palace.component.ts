import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { MemoryPalace } from 'src/app/models/memory-palace';
import { MemoryPalaceRoom } from 'src/app/models/memory-palace-room';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { MemoryPalaceService } from 'src/app/services/memory-palace.service';

@Component({
  selector: 'app-memory-palace',
  templateUrl: './memory-palace.component.html',
  styleUrls: ['./memory-palace.component.scss']
})
export class MemoryPalaceComponent implements OnInit {
  palaceId: string;
  form = new FormGroup({
    name: new FormControl(),
    rooms: new FormArray([])
  });
  get rooms(): FormArray { return this.form.controls.rooms as FormArray; }

  constructor(
    private memoryPalaceService: MemoryPalaceService,
    private route: ActivatedRoute,
    private location: Location,
    private snackBar: MatSnackBar,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.getMemoryPalace();
  }

  getMemoryPalace(): void {
    this.palaceId = this.route.snapshot.paramMap.get('id');

    this.memoryPalaceService.getMemoryPalace(this.palaceId).subscribe(
      memoryPalace => {
        this.form.controls.name.setValue(memoryPalace.name);
        memoryPalace.rooms.forEach(room => this.rooms.push(new FormControl(room.name)));
      },
      error => this.errorHandler.handleError(error)
    );
  }

  addRoom(event: any): void {
    if (event.key === 'Enter' && event.target.value.length > 0) {
      const name = event.target.value;
      this.rooms.push(new FormControl(name));
      event.target.value = '';
    }
  }

  deleteRoom(index: number): void {
    this.rooms.removeAt(index);
  }

  update(): void {
    const palace = new MemoryPalace();
    palace.id = this.palaceId;
    palace.name = this.form.controls.name.value;
    palace.rooms = [];

    for (let i = 0; i < this.rooms.controls.length; i++) {
      const control = this.rooms.controls[i];
      const room = new MemoryPalaceRoom(null, control.value, i);
      palace.rooms.push(room);
    }

    this.memoryPalaceService.updateMemoryPalace(palace).subscribe(
      () => {
        this.goBack();
        this.snackBar.open(`Updated memory palace ${palace.name}`, 'Ok', {
          duration: 2000
        });
      },
      error => this.errorHandler.handleError(error)
    );
  }

  delete(): void {
    this.memoryPalaceService.deleteMemoryPalace(this.palaceId).subscribe(
      () => {
        this.goBack();
        this.snackBar.open(`Deleted memory palace ${this.form.controls.name.value}`, 'Ok', {
          duration: 2000
        });
      },
      error => this.errorHandler.handleError(error)
    );
  }

  goBack(): void {
    this.location.back();
  }
}
