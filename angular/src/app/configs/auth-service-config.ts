import { AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';

import { environment } from './../../environments/environment';

export function getAuthServiceConfig(): AuthServiceConfig {
  const config = new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(environment.googleClientId)
    }
  ]);

  return config;
}
