import { HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, finalize, switchMap, take, tap } from 'rxjs/operators';

import { RefreshTokenRequest } from '../models/refresh-token-request';
import { Token } from '../models/token';
import { HttpService } from './http.service';
import { SessionService } from './session.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class RefreshTokenService {
  private refreshTokenInProgress = false;
  private refreshToken$ = new BehaviorSubject<any>(null);

  constructor(
    private tokenService: TokenService,
    private httpService: HttpService,
    private sessionService: SessionService
  ) { }

  tryRequestWithRefreshedToken(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.refreshToken().pipe(
      switchMap(() => {
        this.refreshToken$.next(true);
        return next.handle(this.addAuthenticationToken(req));
      }),
      finalize(() => {
        this.refreshToken$.next(true);
        this.refreshTokenInProgress = false;
      })
    );
  }

  canTryRefreshToken(error: HttpErrorResponse): boolean {
    return !this.isRefreshExpireError(error) && !!this.tokenService.getRefreshToken();
  }

  waitUntilTokenIsRefreshed(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return this.refreshToken$.pipe(
      filter(result => result !== null),
      take(1),
      switchMap(() => next.handle(this.addAuthenticationToken(req)))
    );
  }

  tokenIsBeingRefreshed(error: HttpErrorResponse): boolean {
    return this.refreshTokenInProgress && !this.isRefreshExpireError(error);
  }

  private isRefreshExpireError(error: HttpErrorResponse): boolean {
    return error.error === 'session_expired';
  }

  private refreshToken(): Observable<Token> {
    this.refreshTokenInProgress = true;
    this.refreshToken$.next(null);
    const payload = new RefreshTokenRequest();
    payload.accessToken = this.tokenService.getSessionToken();
    payload.refreshToken = this.tokenService.getRefreshToken();

    return this.httpService.post('tokens/refresh', payload).pipe(
      tap((token: Token) => {
        this.tokenService.setSessionToken(token.accessToken);
        this.tokenService.setRefreshToken(token.refreshToken);
      }, () => {
        this.tokenService.removeSessionToken();
        this.tokenService.removeRefreshToken();
        this.sessionService.setLoggedInStatus(false);
      })
    );
  }

  private addAuthenticationToken(req: HttpRequest<any>): HttpRequest<any> {
    const accessToken = this.tokenService.getSessionToken();
    return req.clone({ setHeaders: { Authorization: `Bearer ${accessToken}`}});
  }
}
