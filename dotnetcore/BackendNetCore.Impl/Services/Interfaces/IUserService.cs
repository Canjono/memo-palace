using System;

namespace BackendNetCore.Impl.Services.Interfaces
{
    public interface IUserService
    {
        Guid? GetCurrentUserId();
    }
}