import { MemoryPalaceRoom } from './memory-palace-room';

export class MemoryPalaceUpdate {
  name: string;
  rooms: MemoryPalaceRoom[];
}
