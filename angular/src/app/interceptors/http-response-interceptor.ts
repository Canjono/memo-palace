import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { RefreshTokenService } from '../services/refresh-token.service';
import { SessionService } from '../services/session.service';
import { TokenService } from '../services/token.service';

@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor {

  constructor(
    private tokenService: TokenService,
    private refreshTokenService: RefreshTokenService,
    private sessionService: SessionService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          return this.handle401Error(req, next, error);
        }

        return throwError(error);
    }));
  }

  private handle401Error(
    req: HttpRequest<any>,
    next: HttpHandler, error:
    HttpErrorResponse): Observable<any> {
      if (this.refreshTokenService.tokenIsBeingRefreshed(error)) {
        return this.refreshTokenService.waitUntilTokenIsRefreshed(req, next);
      }

      if (!this.refreshTokenService.canTryRefreshToken(error)) {
        return this.refreshTokenService.tryRequestWithRefreshedToken(req, next);
      }

      this.tokenService.removeSessionToken();
      this.tokenService.removeRefreshToken();
      this.sessionService.setLoggedInStatus(false);
      return throwError(error);
  }
}
