import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { MemoryPalace } from 'src/app/models/memory-palace';
import { MemoryPalaceRoom } from 'src/app/models/memory-palace-room';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { MemoryPalaceService } from 'src/app/services/memory-palace.service';

@Component({
  selector: 'app-create-memory-palace',
  templateUrl: './create-memory-palace.component.html',
  styleUrls: ['./create-memory-palace.component.scss']
})
export class CreateMemoryPalaceComponent implements OnInit {
  form = new FormGroup({
    name: new FormControl(null, Validators.required),
    rooms: new FormArray([])
  });
  get rooms(): FormArray { return this.form.controls.rooms as FormArray; }

  constructor(
    private memoryPalaceService: MemoryPalaceService,
    private location: Location,
    private snackBar: MatSnackBar,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() { }

  addRoom(event: any): void {
    if (event.key === 'Enter' && event.target.value.length > 0) {
      const name = event.target.value;
      this.rooms.push(new FormControl(name));
      event.target.value = '';
    }
  }

  deleteRoom(index: number): void {
    this.rooms.removeAt(index);
  }

  create(): void {
    const controls = this.form.controls;
    const palace = new MemoryPalace();
    palace.name = controls.name.value;
    palace.rooms = [];

    for (let i = 0; i < this.rooms.controls.length; i++) {
      const control = this.rooms.controls[i];
      const room = new MemoryPalaceRoom(null, control.value, i);
      palace.rooms.push(room);
    }

    this.memoryPalaceService.createMemoryPalace(palace).subscribe(
      () => {
        this.goBack();
        this.snackBar.open(`Saved memory palace ${palace.name}`, 'Ok', {
          duration: 2000
        });
      },
      error => this.errorHandler.handleError(error)
    );
  }

  goBack(): void {
    this.location.back();
  }
}
