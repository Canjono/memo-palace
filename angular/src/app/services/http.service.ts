import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  get(uri: string): Observable<any> {
    return this.http.get(this.baseUrl + uri);
  }

  post(uri: string, body: any): Observable<any> {
    return this.http.post(this.baseUrl + uri, body);
  }

  put(uri: string, body: any): Observable<any> {
    return this.http.put(this.baseUrl + uri, body);
  }

  delete(uri: string): Observable<any> {
    return this.http.delete(this.baseUrl + uri);
  }

}
