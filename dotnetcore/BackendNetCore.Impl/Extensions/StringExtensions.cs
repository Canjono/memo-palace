using System.Text.RegularExpressions;

namespace BackendNetCore.Impl.Extensions
{
    public static class StringExtensions
    {
        public static string ToSnakeCase(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return input;
            }

            const string pattern = @"([a-z0-9])([A-Z])";
            const string replacement = "$1_$2";

            return Regex.Replace(input, pattern, replacement).ToLower();
        }
    }
}