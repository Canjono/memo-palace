using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Repositories.Interfaces
{
    public interface IWordsRepository
    {
        IEnumerable<Word> GetWords(Guid? userId);
        Word GetWord(Guid wordId, Guid? userId);
        Guid? CreateWord(Word word);
        bool UpdateWord(Word word);
        bool DeleteWord(Guid wordId, Guid? userId);
    }
}