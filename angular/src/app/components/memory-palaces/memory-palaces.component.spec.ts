import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoryPalacesComponent } from './memory-palaces.component';

describe('MemoryPalacesComponent', () => {
  let component: MemoryPalacesComponent;
  let fixture: ComponentFixture<MemoryPalacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemoryPalacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoryPalacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
