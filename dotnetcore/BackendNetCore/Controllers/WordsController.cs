using System;
using System.Collections.Generic;
using System.Linq;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Services.Interfaces;
using BackendNetCore.Models.Requests;
using BackendNetCore.Models.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BackendNetCore.Controllers
{
    [Authorize]
    [Route("api/words")]
    public class WordsController : BaseController
    {
        private readonly IWordService _wordService;
        private readonly IUserService _userService;

        public WordsController(IWordService wordService, IUserService userService)
        {
            _wordService = wordService ?? throw new ArgumentNullException(nameof(wordService));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        [HttpGet("")]
        public ActionResult<IEnumerable<GetWordsResponse>> GetWords()
        {
            var userId = _userService.GetCurrentUserId();
            var words = _wordService.GetWords(userId);

            var wordsDto = words.Select(x => new GetWordsResponse
            {
                CreatedAt = x.CreatedAt,
                Id = x.Id,
                Name = x.Name,
                Translation = x.Translation,
                UpdatedAt = x.UpdatedAt
            });

            return Ok(wordsDto);
        }

        [HttpGet("{id}")]
        public ActionResult<GetWordResponse> GetWord([FromRoute] Guid? id)
        {
            if (id == null)
            {
                return BadRequest("You must send a valid guid/uuid");
            }
            
            var userId = _userService.GetCurrentUserId();
            var word = _wordService.GetWord(id.Value, userId);

            if (word == null)
            {
                return NotFound($"Couldn't find word with id: {id.Value}");
            }

            var wordDto = new GetWordResponse
            {
                CreatedAt = word.CreatedAt,
                Grammar = word.Grammar,
                Id = word.Id,
                Name = word.Name,
                Story = word.Story,
                Translation = word.Translation,
                UpdatedAt = word.UpdatedAt,
                MemoryPalace = word.MemoryPalace == null
                    ? null
                    : new GetWordMemoryPalaceResponse
                    {
                        Id = word.MemoryPalace.Id,
                        Name = word.MemoryPalace.Name,
                        Rooms = word.MemoryPalace.Rooms?.Select(x => new GetMemoryPalaceRoomResponse
                        {
                            Id = x.Id,
                            Index = x.Index,
                            Name = x.Name
                        }).ToList()
                    },
                MemoryPalaceRoom = word.MemoryPalaceRoom == null
                    ? null
                    : new GetWordMemoryPalaceRoomResponse
                    {
                        Id = word.MemoryPalaceRoom.Id,
                        Index = word.MemoryPalaceRoom.Index,
                        Name = word.MemoryPalaceRoom.Name
                    }
            };

            return Ok(wordDto);
        }

        [HttpPost("")]
        public ActionResult<Guid> CreateWord([FromBody] CreateWordRequest request)
        {
            var word = new Word
            {
                Grammar = request.Grammar,
                Name = request.Name,
                Story = request.Story,
                Translation = request.Translation,
                MemoryPalace = request.MemoryPalaceId == null
                    ? null
                    : new MemoryPalace {Id = request.MemoryPalaceId.Value},
                MemoryPalaceRoom = request.MemoryPalaceRoomId == null
                    ? null
                    : new MemoryPalaceRoom {Id = request.MemoryPalaceRoomId.Value},
                UserId = _userService.GetCurrentUserId()
            };

            var id = _wordService.CreateWord(word);

            return id == null
                ? StatusCode(500, "Couldn't create a new word")
                : Ok(id);
        }

        [HttpPut("{id}")]
        public ActionResult UpdateWord([FromRoute] Guid? id, [FromBody] UpdateWordRequest request)
        {
            if (id == null)
            {
                return BadRequest("You must send a valid guid/uuid");
            }

            var word = new Word
            {
                Id = id.Value,
                Name = request.Name,
                Translation = request.Translation,
                Grammar = request.Grammar,
                Story = request.Story,
                MemoryPalace = request.MemoryPalaceId == null
                    ? null
                    : new MemoryPalace
                    {
                        Id = request.MemoryPalaceId.Value
                    },
                MemoryPalaceRoom = request.MemoryPalaceRoomId == null
                    ? null
                    : new MemoryPalaceRoom
                    {
                        Id = request.MemoryPalaceRoomId.Value
                    },
                UserId = _userService.GetCurrentUserId()
            };

            var foundWord = _wordService.UpdateWord(word);

            if (!foundWord)
            {
                return NotFound($"Couldn't find word with id: {id.Value}");
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteWord([FromRoute] Guid? id)
        {
            if (id == null)
            {
                return BadRequest("You must send a valid guid/uuid");
            }

            var userId = _userService.GetCurrentUserId();
            var foundWord = _wordService.DeleteWord(id.Value, userId);

            if (!foundWord)
            {
                return NotFound($"Couldn't find word with id: {id.Value}");
            }

            return Ok();
        }
    }
}