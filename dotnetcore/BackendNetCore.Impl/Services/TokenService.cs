using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;
using BackendNetCore.Impl.Services.Interfaces;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace BackendNetCore.Impl.Services
{
    public class TokenService : ITokenService
    {
        private readonly ITokensRepository _tokensRepository;
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;
        private const int AccessTokenExpirationMinutes = 720;
        private const int RefreshTokenExpirationMinutes = 1440;

        public TokenService(ITokensRepository tokensRepository, IUserService userService, UserManager<User> userManager)
        {
            _tokensRepository = tokensRepository ?? throw new ArgumentNullException(nameof(tokensRepository));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public AuthenticationTokens CreateTokens(User user)
        {
            var accessToken = GenerateAccessToken(user);
            var refreshToken = GenerateRefreshToken();

            var tokens = new AuthenticationTokens
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                CreatedAt = DateTime.UtcNow,
                RefreshExpirationTime = DateTime.UtcNow.AddMinutes(RefreshTokenExpirationMinutes),
                UserId = user.Id
            };

            _tokensRepository.CreateTokens(tokens);

            return tokens;
        }

        public GoogleJsonWebSignature.Payload ValidateGoogleIdToken(string idToken)
        {
            GoogleJsonWebSignature.Payload payload;
            var validationSettings = new GoogleJsonWebSignature.ValidationSettings
            {
                Audience = new[]
                {
                    Environment.GetEnvironmentVariable("CLIENT_ID")
                },
                ExpirationTimeClockTolerance = TimeSpan.FromSeconds(60)
            };

            try
            {
                payload = GoogleJsonWebSignature.ValidateAsync(idToken, validationSettings).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }

            return payload;
        }

        public AuthenticationTokens Refresh(AuthenticationTokens tokens)
        {
            var userId = GetUserIdFromExpiredToken(tokens.AccessToken);

            if (userId == null)
            {
                Console.WriteLine("Couldn't get user id from access token");
                return null;
            }

            var tokensInDb = _tokensRepository.GetTokens(tokens.AccessToken, tokens.RefreshToken, userId);

            if (tokensInDb == null)
            {
                Console.WriteLine("Couldn't find any valid token in db");
                return null;
            }

            var refreshedTokens = CreateTokens(tokensInDb.User);
            _tokensRepository.DeleteTokens(tokensInDb.Id);

            return refreshedTokens;
        }

        public bool DeleteUserTokens()
        {
            var userId = _userService.GetCurrentUserId();

            if (userId == null)
            {
                return false;
            }

            var foundTokens = _tokensRepository.DeleteUserTokens(userId.Value);

            return foundTokens;
        }

        private string GenerateAccessToken(User user)
        {
            var jwtSecret = Environment.GetEnvironmentVariable("JWT_AUTH_SECRET");
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSecret));
            var roles = _userManager.GetRolesAsync(user).Result;
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, roles.FirstOrDefault() ?? "")
            };

            var jwt = new JwtSecurityToken(
                Environment.GetEnvironmentVariable("JWT_ISSUER"),
                Environment.GetEnvironmentVariable("JWT_AUDIENCE"),
                claims: claims,
                DateTime.UtcNow,
                DateTime.UtcNow.AddMinutes(AccessTokenExpirationMinutes),
                new SigningCredentials(key, SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private static string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private static Guid? GetUserIdFromExpiredToken(string token)
        {
            var jwtSecret = Environment.GetEnvironmentVariable("JWT_AUTH_SECRET");
            var issuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSecret));

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidIssuer = Environment.GetEnvironmentVariable("JWT_ISSUER"),
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = issuerSigningKey,
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken;
            ClaimsPrincipal userPrincipal;

            try
            {
                userPrincipal = tokenHandler.ValidateToken(token, tokenValidationParameters, out validatedToken);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }

            if (userPrincipal == null || !IsEncryptedWithExpectedAlgorithm(validatedToken))
            {
                return null;
            }

            return GetUserId(userPrincipal);
        }

        private static bool IsEncryptedWithExpectedAlgorithm(SecurityToken token)
        {
            return token is JwtSecurityToken jwtSecurityToken && jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                       StringComparison.InvariantCultureIgnoreCase);
        }

        private static Guid? GetUserId(ClaimsPrincipal userPrincipal)
        {
            var idClaim = userPrincipal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
            Guid? userId = null;

            if (Guid.TryParse(idClaim?.Value, out var guid))
            {
                userId = guid;
            }

            return userId;
        }
    }
}