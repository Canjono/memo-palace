using System;
using System.Linq;
using System.Security.Claims;
using BackendNetCore.Impl.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace BackendNetCore.Impl.Services
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
        }

        public Guid? GetCurrentUserId()
        {
            var idClaim = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
            var userId = null as Guid?;

            if (Guid.TryParse(idClaim?.Value, out var guid))
            {
                userId = guid;
            }

            return userId;
        }
    }
}