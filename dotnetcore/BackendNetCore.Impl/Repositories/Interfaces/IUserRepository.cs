using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Repositories.Interfaces
{
    public interface IUserRepository
    {
        User GetUser(string username);
    }
}