using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Repositories.Interfaces
{
    public interface IMemoryPalacesRepository
    {
        IEnumerable<MemoryPalace> GetMemoryPalaces(Guid? userId);
        MemoryPalace GetMemoryPalace(Guid palaceId, Guid? userId);
        Guid? CreateMemoryPalace(MemoryPalace memoryPalace);
        bool UpdateMemoryPalace(MemoryPalace memoryPalace);
        bool DeleteMemoryPalace(Guid palaceId, Guid? userId);
        MemoryPalaceRoom GetMemoryPalaceRoom(Guid palaceId);
    }
}