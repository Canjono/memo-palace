﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendNetCore.Impl.Migrations
{
    public partial class AddExpirationDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "expiration_time",
                table: "authentication_tokens",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "expiration_time",
                table: "authentication_tokens");
        }
    }
}
