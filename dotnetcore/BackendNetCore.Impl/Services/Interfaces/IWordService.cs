using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;

namespace BackendNetCore.Impl.Services.Interfaces
{
    public interface IWordService
    {
        IEnumerable<Word> GetWords(Guid? userId);
        Word GetWord(Guid wordId, Guid? userId);
        Guid? CreateWord(Word word);
        bool UpdateWord(Word word);
        bool DeleteWord(Guid wordId, Guid? userid);
    }
}