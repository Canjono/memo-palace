import { Moment } from 'moment';

import { MemoryPalace } from './memory-palace';
import { MemoryPalaceRoom } from './memory-palace-room';

export class Word {
  id: string;
  name: string;
  translation: string;
  grammar: string;
  story: string;
  createdAt: Moment;
  updatedAt: Moment;
  memoryPalace: MemoryPalace;
  memoryPalaceRoom: MemoryPalaceRoom;
}
