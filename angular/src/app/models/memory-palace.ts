import { MemoryPalaceRoom } from './memory-palace-room';

export class MemoryPalace {
  id: string;
  name: string;
  rooms: MemoryPalaceRoom[];
}
