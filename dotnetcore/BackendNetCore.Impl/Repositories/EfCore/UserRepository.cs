using System;
using System.Linq;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;

namespace BackendNetCore.Impl.Repositories.EfCore
{
    public class UserRepository : IUserRepository
    {
        private readonly VocabularyContext _context;

        public UserRepository(VocabularyContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public User GetUser(string id)
        {
            return _context.Users.FirstOrDefault(x => x.ExternalId == id);
        }
    }
}