import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { MemoryPalace } from 'src/app/models/memory-palace';
import { Word } from 'src/app/models/word';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { MemoryPalaceService } from 'src/app/services/memory-palace.service';
import { WordService } from 'src/app/services/word.service';

@Component({
  selector: 'app-create-word',
  templateUrl: './create-word.component.html',
  styleUrls: ['./create-word.component.scss']
})
export class CreateWordComponent implements OnInit {
  memoryPalaces: MemoryPalace[] = [];
  form = new FormGroup({
    name: new FormControl(null, Validators.required),
    translation: new FormControl(null, Validators.required),
    grammar: new FormControl(),
    story: new FormControl(),
    memoryPalace: new FormControl(),
    memoryPalaceRoom: new FormControl()
  });

  constructor(
    private memoryPalaceService: MemoryPalaceService,
    private wordService: WordService,
    private location: Location,
    private snackBar: MatSnackBar,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.getMemoryPalaces();
  }

  getMemoryPalaces(): void {
    this.memoryPalaceService.getMemoryPalaces().subscribe(
      memoryPalaces => this.memoryPalaces = memoryPalaces,
      error => this.errorHandler.handleError(error)
    );
  }

  create(): void {
    const controls = this.form.controls;
    const word = new Word();
    word.name = controls.name.value;
    word.translation = controls.translation.value;
    word.grammar = controls.grammar.value;
    word.story = controls.story.value;
    word.memoryPalace = controls.memoryPalace.value;
    word.memoryPalaceRoom = controls.memoryPalaceRoom.value;

    this.wordService.createWord(word).subscribe(
      () => {
        this.goBack();
        this.snackBar.open(`Created word ${word.name}`, 'Ok', {
          duration: 2000
        });
      },
      error => this.errorHandler.handleError(error)
    );
  }

  reset(): void {
    this.form.reset();
  }

  goBack(): void {
    this.location.back();
  }

}
