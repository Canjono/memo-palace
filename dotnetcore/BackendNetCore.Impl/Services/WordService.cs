using System;
using System.Collections.Generic;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;
using BackendNetCore.Impl.Services.Interfaces;

namespace BackendNetCore.Impl.Services
{
    public class WordService : IWordService
    {
        private readonly IWordsRepository _wordsRepository;
        private readonly IMemoryPalacesRepository _memoryPalacesRepository;
        private readonly IUserService _userService;

        public WordService(IWordsRepository wordsRepository, IMemoryPalacesRepository memoryPalacesRepository, IUserService userService)
        {
            _wordsRepository = wordsRepository ?? throw new ArgumentNullException(nameof(wordsRepository));
            _memoryPalacesRepository = memoryPalacesRepository ??
                                       throw new ArgumentNullException(nameof(memoryPalacesRepository));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        public IEnumerable<Word> GetWords(Guid? userId)
        {
            var words = _wordsRepository.GetWords(userId);

            return words;
        }

        public Word GetWord(Guid wordId, Guid? userId)
        {
            var word = _wordsRepository.GetWord(wordId, userId);

            return word;
        }

        public Guid? CreateWord(Word word)
        {
            var memoryPalace = word.MemoryPalace == null
                ? null
                : _memoryPalacesRepository.GetMemoryPalace(word.MemoryPalace.Id, word.UserId);
            var memoryPalaceRoom = word.MemoryPalaceRoom == null
                ? null
                : _memoryPalacesRepository.GetMemoryPalaceRoom(word.MemoryPalaceRoom.Id);
            
            word.CreatedAt = DateTime.UtcNow;
            word.UpdatedAt = DateTime.UtcNow;
            word.MemoryPalace = memoryPalace;
            word.MemoryPalaceRoom = memoryPalaceRoom;

            var wordId = _wordsRepository.CreateWord(word);

            return wordId;
        }

        public bool UpdateWord(Word word)
        {
            word.UpdatedAt = DateTime.UtcNow;

            var foundWord = _wordsRepository.UpdateWord(word);

            return foundWord;
        }

        public bool DeleteWord(Guid wordId, Guid? userId)
        {
            var foundWord = _wordsRepository.DeleteWord(wordId, userId);

            return foundWord;
        }
    }
}