using System;
using System.Linq;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BackendNetCore.Impl.Repositories.EfCore
{
    public class TokensRepository : ITokensRepository
    {
        private readonly VocabularyContext _context;

        public TokensRepository(VocabularyContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public bool CreateTokens(AuthenticationTokens tokens)
        {
            _context.AuthenticationTokens.Add(tokens);
            _context.SaveChanges();

            return true;
        }

        public AuthenticationTokens GetTokens(string accessToken, string refreshToken, Guid? userId)
        {
            var now = DateTime.UtcNow;

            var tokens = _context.AuthenticationTokens
                .Include(x => x.User)
                .FirstOrDefault(x =>
                    x.UserId == userId && x.AccessToken == accessToken && x.RefreshToken == refreshToken &&
                    x.RefreshExpirationTime > now);

            return tokens;
        }

        public bool DeleteTokens(Guid id)
        {
            var tokens = _context.AuthenticationTokens.FirstOrDefault(x => x.Id == id);

            if (tokens == null)
            {
                return false;
            }

            _context.AuthenticationTokens.Remove(tokens);
            _context.SaveChanges();

            return true;
        }

        public bool DeleteUserTokens(Guid userId)
        {
            var tokens = _context.AuthenticationTokens.Where(x => x.UserId == userId).ToList();

            if (!tokens.Any())
            {
                return false;
            }
            
            _context.AuthenticationTokens.RemoveRange(tokens);
            _context.SaveChanges();

            return true;
        }
    }
}