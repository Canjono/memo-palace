using System;
using BackendNetCore.Impl.Services.Interfaces;
using BackendNetCore.Models.Requests;
using BackendNetCore.Models.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BackendNetCore.Controllers
{
    [Route("api/authentication")]
    public class AuthenticationController : BaseController
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService =
                authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
        }

        [HttpPost("login/google")]
        public ActionResult<LoginGoogleResponse> Login([FromBody] LoginGoogleRequest request)
        {
            var tokens = _authenticationService.AuthenticateGoogle(request.IdToken);

            if (tokens == null)
            {
                return BadRequest();
            }

            var loginGoogleDto = new LoginGoogleResponse
            {
                AccessToken = tokens.AccessToken,
                RefreshToken = tokens.RefreshToken
            };

            return Ok(loginGoogleDto);
        }

        [Authorize]
        [HttpPost("logout")]
        public ActionResult Logout()
        {
            _authenticationService.Logout();

            return Ok();
        }
    }
}