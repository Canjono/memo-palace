import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  sessionTokenKey = 'sessionToken';
  refreshTokenKey = 'refreshToken';
  usernameKey = 'username';

  private isLoggedInSubject = new Subject();

  constructor(private httpService: HttpService) { }

  login(idToken: string): Observable<any> {
    return this.httpService.post('authentication/login/google', { idToken });
  }

  logout(): Observable<any> {
    return this.httpService.post('authentication/logout', {}).pipe(
      tap(() => {
        localStorage.removeItem(this.sessionTokenKey);
        localStorage.removeItem(this.refreshTokenKey);
        localStorage.removeItem(this.usernameKey);
        this.isLoggedInSubject.next(false);
      })

    );
  }

  getUsername(): string {
    return localStorage.getItem(this.usernameKey);
  }

  setUsername(username: string): void {
    localStorage.setItem(this.usernameKey, username);
  }

}
