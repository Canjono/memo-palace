using System;
using System.Collections.Generic;
using System.Linq;
using BackendNetCore.Impl.Models;
using BackendNetCore.Impl.Repositories.Interfaces;
using BackendNetCore.Impl.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BackendNetCore.Impl.Repositories.EfCore
{
    public class MemoryPalacesRepository : IMemoryPalacesRepository
    {
        private readonly VocabularyContext _context;

        public MemoryPalacesRepository(VocabularyContext context, IUserService userService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<MemoryPalace> GetMemoryPalaces(Guid? userId)
        {
            var memoryPalaces = _context.MemoryPalaces
                .Include(x => x.Rooms)
                .Where(x => x.UserId == userId)
                .ToList();

            return memoryPalaces;
        }

        public MemoryPalace GetMemoryPalace(Guid palaceId, Guid? userId)
        {
            var memoryPalace = _context.MemoryPalaces
                .Include(x => x.Rooms)
                .FirstOrDefault(x => x.Id == palaceId && x.UserId == userId);

            return memoryPalace;
        }

        public Guid? CreateMemoryPalace(MemoryPalace memoryPalace)
        {
            _context.MemoryPalaces.Add(memoryPalace);
            _context.SaveChanges();

            return memoryPalace.Id;
        }

        public bool UpdateMemoryPalace(MemoryPalace memoryPalace)
        {
            var dbMemoryPalace = _context.MemoryPalaces
                .Include(x => x.Rooms)
                .FirstOrDefault(x => x.Id == memoryPalace.Id && x.UserId == memoryPalace.UserId);

            if (dbMemoryPalace == null)
            {
                return false;
            }

            dbMemoryPalace.Name = memoryPalace.Name;

            foreach (var room in memoryPalace.Rooms)
            {
                if (room.Id == Guid.Empty)
                {
                    dbMemoryPalace.Rooms.Add(room);
                    continue;
                }

                var existingRoom = dbMemoryPalace.Rooms.FirstOrDefault(x => x.Id == room.Id);

                if (existingRoom == null)
                {
                    throw new Exception("One of the room ids did not belong to the memory palace");
                }

                existingRoom.Index = room.Index;
                existingRoom.Name = room.Name;
            }

            var roomsToDelete = dbMemoryPalace.Rooms.Where(x => memoryPalace.Rooms.All(y => y.Id != x.Id));
            var wordsWithDeletedRooms = _context.Words
                .Include(x => x.MemoryPalace)
                .Include(x => x.MemoryPalaceRoom)
                .Where(x => roomsToDelete.Any(y => y.Id == x.MemoryPalaceRoom.Id))
                .ToList();

            wordsWithDeletedRooms.ForEach(x =>
            {
                x.MemoryPalace = null;
                x.MemoryPalaceRoom = null;
            });

            _context.MemoryPalaceRooms.RemoveRange(roomsToDelete);

            _context.SaveChanges();

            return true;
        }

        public bool DeleteMemoryPalace(Guid palaceId, Guid? userId)
        {
            var memoryPalace = _context.MemoryPalaces
                .Include(x => x.Rooms)
                .FirstOrDefault(x => x.Id == palaceId && x.UserId == userId);

            if (memoryPalace == null)
            {
                return false;
            }

            var wordsWithDeletedPalace = _context.Words
                .Include(x => x.MemoryPalace)
                .Include(x => x.MemoryPalaceRoom)
                .Where(x => memoryPalace.Id == x.MemoryPalace.Id)
                .ToList();

            wordsWithDeletedPalace.ForEach(x =>
            {
                x.MemoryPalace = null;
                x.MemoryPalaceRoom = null;
            });

            _context.MemoryPalaceRooms.RemoveRange(memoryPalace.Rooms);
            _context.MemoryPalaces.Remove(memoryPalace);

            _context.SaveChanges();

            return true;
        }

        public MemoryPalaceRoom GetMemoryPalaceRoom(Guid id)
        {
            var room = _context.MemoryPalaceRooms.FirstOrDefault(x => x.Id == id);

            return room;
        }
    }
}