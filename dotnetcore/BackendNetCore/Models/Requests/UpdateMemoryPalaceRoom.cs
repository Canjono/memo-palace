using System;

namespace BackendNetCore.Models.Requests
{
    public class UpdateMemoryPalaceRoom
    {
        public Guid? Id { get; set; }
        public int Index { get; set; }
        public string Name { get; set; }
    }
}