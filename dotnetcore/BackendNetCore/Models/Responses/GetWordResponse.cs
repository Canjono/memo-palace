using System;

namespace BackendNetCore.Models.Responses
{
    public class GetWordResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Translation { get; set; }
        public string Grammar { get; set; }
        public string Story { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public GetWordMemoryPalaceResponse MemoryPalace { get; set; }
        public GetWordMemoryPalaceRoomResponse MemoryPalaceRoom { get; set; }
    }
}