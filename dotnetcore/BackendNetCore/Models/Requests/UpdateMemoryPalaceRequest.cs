using System.Collections.Generic;

namespace BackendNetCore.Models.Requests
{
    public class UpdateMemoryPalaceRequest
    {
        public string Name { get; set; }
        public List<UpdateMemoryPalaceRoom> Rooms { get; set; }
    }
}