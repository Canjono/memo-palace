using System.Collections.Generic;

namespace BackendNetCore.Models.Requests
{
    public class CreateMemoryPalaceRequest
    {
        public string Name { get; set; }
        public List<string> Rooms { get; set; }
    }
}